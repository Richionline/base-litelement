# Ejemplo de Dockerfile realizado en la clase
FROM nginx:1.11-alpine

# COPY <src> <dest> allows you to copy files from the directory containing the Dockerfile to the container's image
COPY index.html /usr/share/nginx/html

# Using the EXPOSE <port> command you tell Docker which ports should be open and can be bound to
EXPOSE 80

# The CMD line in a Dockerfile defines the default command to run when a container is launched. 
# If the command requires arguments then it's recommended to use an array
CMD ["nginx", "-g", "daemon off;"]


# Comandos para crear contenedor y ejecutar
# Creamos docker
# docker build -t my-nginx-image:latest .

# Ejecutamos docker
# docker run -d -p 80:80 my-nginx-image:latest

# Para probarlo
# curl -i http://docker