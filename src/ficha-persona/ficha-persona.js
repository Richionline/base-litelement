import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        }
    }

    constructor() {
        super();

        this.name = "Prueba nombre"
        this.yearsInCompany = 2
        this.updatePersonInfo()
    }

    render() {
        return html`
            <div><h1>Ficha persona</h1></div>
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}" @change="${this.updatePersonInfo}"></input>
                <br />
                <label>Personal Info</label>
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
            </div>   
        `;
    }       

    // Se llama cuando se ha pintado y actualizado el componente
    updated(changedProperties) {
        console.log("updated")
        
        /*
        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue);
        });
        */

        if (changedProperties.has("name")) {
            console.log("Propiedad name cambia valor anterior era " + changedProperties.get("name")
                + " nuevo es " + this.name);
        }        

        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany cambia valor anterior era " + changedProperties.get("yearsInCompany")
                + " nuevo es " + this.yearsInCompany);
            this.updatePersonInfo()
        }
    }

    updateName(e) {
        console.log("updateName")
        this.name = e.target.value  // e es un objeto de evento que es la informacion del evento nativo
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany")
        this.yearsInCompany = e.target.value
        // this.updatePersonInfo() // Es mejor poner en el metodo standard del ciclo de vida updated(changedProperties) y asi desacoplarlo
    }

    updatePersonInfo() {
        console.log("updatePersonInfo")
        console.log("yearsInCompany vale " + this.yearsInCompany)

        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead"
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior"
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team"
        } else {
            this.personInfo = "junior"
        }
    }
}

customElements.define('ficha-persona', FichaPersona);