import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        }
    }

    constructor() {
        super()

        this.resetFormData();
        this.editingPerson = false
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div>
                <form">
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input type="text" 
                                .value="${this.person.name}" 
                                ?disabled="${this.editingPerson}" 
                                class="form-control" 
                                placeholder="Nombre completo" 
                                @change="${this.updateName}"/>
                    </div>                    
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5" @change="${this.updateProfile}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" .value="${this.person.yearsInCompany}" class="form-control" placeholder="Años en la empresa" @change="${this.updateYearsInCompany}"/>
                    </div>
                    <button class="btn btn-primary" @click="${this.goBack}"><strong>Atrás</strong></button>
                    <button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }       

    updateName(e) {
        console.log("updateName")
        console.log("Actualizando valor de name con " + e.target.value)
        this.person.name = e.target.value
    }
    
    updateProfile(e) {
        console.log("updateProfile")
        console.log("Actualizando valor de profile con " + e.target.value)
        this.person.profile = e.target.value
    }
    
    updateYearsInCompany(e) {
        console.log("updateYearsInCompany")
        console.log("Actualizando valor de yearsInCompany con " + e.target.value)
        this.person.yearsInCompany = e.target.value
    }

    goBack(e) {
        console.log("goBack en persona-form")
        e.preventDefault() // Evitamos comportamiento normal del boton en formulario

        this.resetFormData();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}))        
    }

    storePerson(e) {
        console.log("storePerson en persona-form")
        e.preventDefault()

        // Metemos una foto por defecto
        this.person.photo = {
            src: "./img/persona.png",
            alt: "Este es " + this.person.name,
        }

        console.log("name a guardar: " + this.person.name)
        console.log("profile a guardar: " + this.person.profile)
        console.log("yearsInCompany a guardar: " + this.person.yearsInCompany)

        this.dispatchEvent(
            new CustomEvent("persona-form-store", {
                detail: {
                    person: {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    },
                    editingPerson : this.editingPerson,
                }
            })
        )        
    }

    resetFormData() {
        console.log("resetFormData")

        this.person = {}
        this.person.name = ""
        this.person.profile = ""
        this.person.yearsInCompany = ""

        this.editingPerson = false
    }
}

customElements.define('persona-form', PersonaForm);
