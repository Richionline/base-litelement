import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super()

        this.people = [];
    }

    updated(changedProperties) {
        console.log("updated en persona-stats")

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-stats: " + this.people.length)

            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people-stats", {
                    detail: {
                        peopleStats: peopleStats
                    }
                }
            ))
        }
    }       

    gatherPeopleArrayInfo() {
        console.log("gatherPeopleArrayInfo");

        let peopleStats = {};
        peopleStats.numberOfPeople = this.people.length;
        console.log("NUMERO " + peopleStats.numberOfPeople)

        return peopleStats;
    }
}

customElements.define('persona-stats', PersonaStats);
