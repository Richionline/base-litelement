import { LitElement, html } from 'lit-element';

class TestApi extends LitElement {

    static get properties() {
        return {
            movies: {type: Array},
        }
    }

    constructor() {
        super();

        this.movies = [];
        this.getMovieData();
    }

    render() {
        return html`
            <h1>Test API</h1>
            ${this.movies.map(
                movie => html`<div>La película ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
        `;
    }       

    getMovieData() {
        console.log("getMovieData")
        console.log("Obteniendo datos de las pelis")

        // Usamos AJAX
        let xhr = new XMLHttpRequest();

        // onload signifcica que se ha lanzado la petición y ha llegado, en este caso lo que estoy haciendo es 
        // cuando se finaliza la petición se ejecuta la función anonima ()
        // Funcion anonima que no tiene nombre, por eso se pone () nada más, podria ser ... = hola() => {...}
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente")

                let APIResponse = JSON.parse(xhr.responseText);

                // El campo que nos interesa de esa api se llama results
                this.movies = APIResponse.results

                console.log("Cargadas películas: " + this.movies)
            }
        }

        // open no envia la petición, es como una especia de init
        xhr.open("GET", "https://swapi.dev/api/films/");
        // Hacemos la petición
        xhr.send();

        console.log("Fin de getMovieData")
    }

}

customElements.define('test-api', TestApi);
