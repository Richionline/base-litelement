import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js' // No haría falta poner .js
import '../persona-main/persona-main' 
import '../persona-footer/persona-footer'
import '../persona-sidebar/persona-sidebar'
import '../persona-stats/persona-stats'

class PersonaApp extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        }
    }

    constructor() {
        super()

        this.people = []
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
                <persona-main class="col-10" @update-people="${this.updatePeople}"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.peopleStatsUpdated}"></persona-stats>
        `;
    }       

    updated(changeProperties) {
        console.log("updated en persona-app")

        if (changeProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-app: " + this.people.length)
            this.shadowRoot.querySelector("persona-stats").people = this.people
        }
    }

    newPerson(e) {
        console.log("newPerson en persona-app")
        console.log(e)

        this.shadowRoot.querySelector("persona-main").showPersonForm = true
    }

    updatePeople(e) {
        console.log("updatePeople")

        this.people = e.detail.people;
    }

    peopleStatsUpdated(e) {
        console.log("peopleStatsUpdated")

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }

}

customElements.define('persona-app', PersonaApp);
