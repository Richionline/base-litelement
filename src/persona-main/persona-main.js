import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado'
import '../persona-form/persona-form'

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
        }
    }

    constructor() {
        super()

        this.showPersonForm = false

        this.people = [
            {
                name: "Richi",
                yearsInCompany: 10,
                profile: "Loren ipsum dolor sit amet.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Este es Richi",
                },
            }, {
                name: "Lola",
                yearsInCompany: 2,
                profile: "Loren ipsum dolor sit.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Este es Lola",
                },
            }, {
                name: "Pedro",
                yearsInCompany: 5, 
                profile: "Loren ipsum dolor.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Este es Pedro",
                },
            }, {
                name: "Vicky",
                yearsInCompany: 5, 
                profile: "Loren ipsum.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Este es Vicky",
                },
            }, {
                name: "Juan",
                yearsInCompany: 5, 
                profile: "Loren.",
                photo: {
                    src: "./img/persona.png",
                    alt: "Este es Juan",
                },
            },
        ];
    }

    // Atención al . para el objeto photo (.photo="${person.photo}")
    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`
                        <persona-ficha-listado 
                            fname="${person.name}"
                            yearsInCompany="${person.yearsInCompany}"
                            profile="${person.profile}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}"
                        >
                        </persona-ficha-listado>`
                )} 
                </div>
            </div>
            <div class="row">
                <persona-form class="d-none border rounded border-primary" id="personForm" 
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}">
                </persona-form>
            </div>
        `;
    }     
    
    personFormClose() {
        this.showPersonForm = false
    }

    personFormStore(e) {
        console.log("personFormStore")

        console.log(e.detail.person)
        console.log("La propiedad editingPerson vale: " + e.detail.editingPerson)

        // Comprobamos si viene de un alta o es un update
        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona " + e.detail.person.name)

            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            )
            /*
            // findIndex devuelve el indice del array donde se ha cumplido el primer true, donde localiza el primer elemento
            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );

            if (indexOfPerson !== -1) {
                console.log("Encontrado registro a modificar en array")
                this.people[indexOfPerson] = e.detail.person
            }
            */

        } else {
            console.log("Se va a almacenar una nueva persona")
            
            this.people = [...this.people, e.detail.person];
            // this.people.push(e.detail.person)
        }

        this.showPersonForm = false
    }

    // Se ejecuta cada vez que se actualiza la página
    updated(changedProperties) {
        console.log("updated en persona-main")

        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propieda showPersonForm en persona-main")

            if (this.showPersonForm === true) {
                this.showPersonFormData()
            } else {
                this.showPersonList()
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en person-main")

            this.dispatchEvent(new CustomEvent("update-people", {
                    "detail": {
                        people: this.people
                    }
                }
            ));
        }

    }

    showPersonFormData() {
        console.log('showPersonFormData')
        console.log("Mostrando formulario de persona")

        this.shadowRoot.getElementById("personForm").classList.remove("d-none")
        this.shadowRoot.getElementById("peopleList").classList.add("d-none")
    }

    showPersonList() {
        console.log("showPersonList")
        console.log("Mostrando el listado de personas")

        this.shadowRoot.getElementById("personForm").classList.add("d-none")
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none")
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main")
        console.log("Se va a borrar a la persona " + e.detail.name)

        this.people = this.people.filter(
            person =>  person.name != e.detail.name
        ) 
    }

    infoPerson(e) {
        console.log("infoPerson en persona-main")
        console.log("Se va a obtener la info de la persona " + e.detail.name)

        let chosenPerson = this.people.filter(
            person =>  person.name === e.detail.name
        ) 

        console.log("Persona encontrada: " + chosenPerson[0].name)
        console.log(chosenPerson)

        // Esta pasando la referencia del objeto por lo que actualizamos directamente el array
        // this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
        // Por ello mejor usamos variable intermedia
        let personToShow = {}
        personToShow.name = chosenPerson[0].name
        personToShow.profile = chosenPerson[0].profile
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany
        this.shadowRoot.getElementById("personForm").person = personToShow;

        this.shadowRoot.getElementById("personForm").editingPerson = true;

        this.showPersonForm = true;
    }    
}

customElements.define('persona-main', PersonaMain);
